from urllib import request
from bs4 import BeautifulSoup
import json
import boto3


def fetch_data_from_marketshare():
    url = 'https://www.marketwatch.com/tools/marketsummary?region=usa&screener=nasdaq'
    beautiful = request.urlopen(url).read()
    soup = BeautifulSoup(beautiful, 'html.parser')

    results = soup.find_all(id='marketscreenerresults')
    scrapedData = []
    for result in results:
        rows = result.find_all('tr')
        for row in rows:
            if row.get('class') == ['understated']:
                continue

            row_result = {
                'name': row.findAll('td')[0].a.string,
                'title': row.findAll('td')[0].a['title'],
                'value': row.findAll('td')[1].string,
                'change': row.findAll('td')[2].string,
                'changePct': row.findAll('td')[3].string
            }

            scrapedData.append(row_result)

    return scrapedData


def save_to_params(scrapedData):
    ssm = boto3.client('ssm')
    response = ssm.put_parameter(
        Name='scrapedData',
        Value=json.dumps(scrapedData),
        Type='String',
        Overwrite=True
    )
    print(response)

    return


def fetch_active_stocks(event, context):
    data = fetch_data_from_marketshare()
    save_to_params(data)
    return {
        'statusCode': 200,
        'body': 'ok'
    }
