import alpaca_trade_api as tradeapi
import json
import boto3

def get_market_value(event, context):
    api = tradeapi.REST()
    account = api.get_account()
    symbols = get_symbols_from_params()
    print(symbols)
    return symbols

    list_pos = api.get_barset(
        symbols=event['symbols'],
        timeframe='day'
        )
    json_pos_list = {}
    for stock, bars in list_pos.items():
        json_pos_list.update({stock : json.dumps(bars.__dict__)})

    return {
        'statusCode': 200,
        'bars': json_pos_list
    }

def get_symbols_from_params():
    ssm = boto3.client('ssm')
    response = ssm.get_parameter(
        Name='scrapedData'
    )

    return response.Parameter.Value