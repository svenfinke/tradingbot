include .env

.PHONY=deploy local validate destroy status init all

BUCKET=sf-2020-code
STACK=tradingbot
IP_ADDR := $(shell /sbin/ifconfig eth0 | grep 'inet ' | awk '{print $$2}')
ENV_VARS := $(shell echo $$(cut -d= -f1 .env) | xargs echo '' | sed "s/ /$$/g")

# Variables for generating the .env file
TMP_AWS_ACCOUNT_ID := $(shell aws sts get-caller-identity | jq -r ".Account")
TMP_AWS_REGION := $(shell aws configure get region)
TMP_AWS_ACCESS_KEY_ID := $(shell aws configure get aws_access_key_id)
TMP_AWS_SECRET_ACCESS_KEY := $(shell aws configure get aws_secret_access_key)
TMP_LAMBDA_ENDPOINT := http://${IP_ADDR}:3001/

all: _create_template validate local
deploy: _create_template _build _package _deploy
local: _build _run_local
validate: _validate
init: _create_env _check_deps
status: _status
destroy: _destroy

_create_template:
	@echo ' > Add environment vars to cf template'
	@$(cat .env | awk '{print "export "$1}')
	@envsubst '${ENV_VARS}' < template.raw.yaml > template.yaml
	@echo ' > Done'

_validate:
	@echo ' > Start validation'
	@sam validate
	@echo ' > Done'

_run_local:
	@make -j2 _local

_local: _local_lambda _local_step_functions

_local_lambda:
	@echo ' > Starting Lambda server'
	@sam local start-lambda --host ${IP_ADDR}
	@echo ' > Terminated Lambda server'

_local_step_functions:
	@echo ' > Starting Stepfunction server'
	@docker run -p 8083:8083 --env-file .env amazon/aws-stepfunctions-local
	@echo ' > Terminated Stepfunction server'

_build:
	@echo ' > Build functions'
	@sam build
	@echo ' > Done'

_package:
	@echo ' > Package functions and template files'
	@sam package --s3-bucket ${BUCKET} --output-template-file dist/package.yaml
	@echo ' > Done'

_deploy:
	@echo ' > Deploy Cloudformation Template'
	@sam deploy --template-file dist/package.yaml --stack-name ${STACK} --capabilities CAPABILITY_IAM
	@echo ' > Done'

_destroy:
	@echo ' > Delete Cloudformation Stack'
	@aws cloudformation delete-stack --stack-name ${STACK}
	@echo ' > Done'

_status:
	@echo ' > Get Cloudformation Stack Status'
	@aws cloudformation describe-stacks --stack-name ${STACK}

_create_env:
ifneq (,$(wildcard ./.env))
	@echo ' ERR > File already exists!'
else
	@echo ' > Copy environment file template'
	@cp .env.dist .env
	@echo ' > Set Variables in the environment file. The values are read from your AWS Config.'
	@sed -i 's/AWS_ACCOUNT_ID_VALUE/${TMP_AWS_ACCOUNT_ID}/g' .env
	@sed -i 's/AWS_ACCESS_KEY_ID_VALUE/${TMP_AWS_ACCESS_KEY_ID}/g' .env
	@sed -i "s~AWS_SECRET_ACCESS_KEY_VALUE~${TMP_AWS_SECRET_ACCESS_KEY}~g" .env
	@sed -i "s/AWS_REGION_VALUE/${TMP_AWS_REGION}/g" .env
	@sed -i "s~LAMBDA_ENDPOINT_VALUE~${TMP_LAMBDA_ENDPOINT}~g" .env
	@echo ' > Done.'
endif

_check_deps:
	@echo Checking for required tools:
	@echo  - Docker
	@echo  - AWS CLI
	@echo  - AWS SAM CLI
	@echo [Add checks]